<?php
    require '../includes/database.php';
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: list.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'SELECT r.ID, r.RENDELESI_MENNYISEG, a.ID ALKATRESZ_ID, a.MEGNEVEZES ALKATRESZ, a.LEIRAS, '
            .'a.RENDELESI_KOD, b.NEV BESZALLITO, b.ID BESZALLITO_ID '
            .'FROM RENDELES r '
            .'JOIN ALKATRESZ a ON r.ALKATRESZ_ID = a.ID '
            .'JOIN BESZALLITO b ON a.BESZALLITO_ID = b.ID '
            .'WHERE r.ID = ?';
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
    }
?>

<?php include("../includes/header.php"); ?>

<div class="app-rendeles">
    <div class="row app-page-title"><h3>Rendelés</h3></div>
        
    <div class="app-page-body form-horizontal" >
        <div class="control-group">
        <label class="control-label">Azonosító #</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['ID'];?>
            </label>
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Alkatrész</label>
        <div class="controls">
            <a class="checkbox" href="../alkatresz/show.php?id=<?php echo $data['ALKATRESZ_ID'];?>"><?php echo $data['ALKATRESZ'];?></a>
        </div>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['LEIRAS'];?>
            </label>
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Beszállító</label>
        <div class="controls">
            <a class="checkbox" href="../beszallito/show.php?id=<?php echo $data['BESZALLITO_ID'];?>"><?php echo $data['BESZALLITO'];?></a>                
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Rendelési kód</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['RENDELESI_KOD'];?>
            </label>
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Rendelési mennyiség</label>
        <div class="controls">
            <label class="checkbox">
                <strong><?php echo $data['RENDELESI_MENNYISEG'];?></strong>
            </label>
        </div>
        </div>

        <div class="form-actions app-page-actions">
            <a class="btn btn-success" href="update.php?id=<?php echo $_GET['id'];?>">Módosít</a>
            <a class="btn" onclick="history.go(-1);">Vissza</a>
        </div>
    </div>
</div>

<?php include("../includes/footer.php"); ?>
