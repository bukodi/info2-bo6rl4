<?php
    require '../includes/database.php';

    $id = 0;
     
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( !empty($_POST)) {
        // keep track post values
        $id = $_POST['id'];
         
        // delete data
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "DELETE FROM RENDELES WHERE id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        Database::disconnect();
        header("Location: list.php");
    }
?>

<?php include("../includes/header.php"); ?>

<div class="app-rendeles">
    <div class="row app-page-title"><h3>Beszállító tötlése</h3></div>
             
    <form class="app-page-body form-horizontal" action="delete.php" method="post">
        <input type="hidden" name="id" value="<?php echo $id;?>"/>
        <p class="alert alert-error">Biztosan törölni akarja a rendelést ?</p>
        <div class="form-actions app-page-actions">
            <button type="submit" class="btn btn-danger">Igen</button>
            <a class="btn" href="list.php">Nem</a>
        </div>
    </form>
</div>

<?php include("../includes/footer.php"); ?>
