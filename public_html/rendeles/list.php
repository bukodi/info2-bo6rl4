<?php include("../includes/header.php"); ?>
<div class="app-rendeles">
    <div class="row app-page-title"><h3>Rendelések listája</h3></div>
    <div class="row app-page-body">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Alkatrész</th>
                <th>Beszállító</th>
                <th>Rendelési kód</th>
                <th>Mennyiség</th>
                <th><a href="create.php" class="btn btn-success">Új rendelés</a></th>
            </tr>
            </thead>
            <tbody>
            <?php
            include '../includes/database.php';
            $pdo = Database::connect();
            $sql = 'SELECT r.ID, r.RENDELESI_MENNYISEG, a.ID ALKATRESZ_ID, a.MEGNEVEZES ALKATRESZ, '
                .'a.RENDELESI_KOD, b.NEV BESZALLITO, b.ID BESZALLITO_ID '
                .'FROM RENDELES r '
                .'JOIN ALKATRESZ a ON r.ALKATRESZ_ID = a.ID '
                .'JOIN BESZALLITO b ON a.BESZALLITO_ID = b.ID '
                . (empty($_GET['beszallito_id']) ?'':' WHERE a.BESZALLITO_ID = '.$_GET['beszallito_id'])
                . (empty($_GET['alkatresz_id']) ?'':' WHERE a.ID = '.$_GET['alkatresz_id'])
                .' ORDER BY r.ID';
            foreach ($pdo->query($sql) as $row) {
                    echo '<tr>'."\r\n";
                    echo '  <td>'. $row['ID'] . '</td>'."\r\n";
                    echo '  <td><a href="../alkatresz/show.php?id='.$row['ALKATRESZ_ID'].'">'.$row['ALKATRESZ'].'</a></td>'."\r\n";
                    echo '  <td><a href="../beszallito/show.php?id='.$row['BESZALLITO_ID'].'">'.$row['BESZALLITO'].'</a></td>'."\r\n";
                    echo '  <td>'. $row['RENDELESI_KOD'] . '</td>'."\r\n";
                    echo '  <td>'. $row['RENDELESI_MENNYISEG'] . '</td>'."\r\n";
                    echo '  <td width=250>';
                    echo '<a class="btn" href="show.php?id='.$row['ID'].'">Megnéz</a>';
                    echo ' ';
                    echo '<a class="btn btn-success" href="update.php?id='.$row['ID'].'">Módosít</a>';
                    echo ' ';
                    echo '<a class="btn btn-danger" href="delete.php?id='.$row['ID'].'">Töröl</a>';
                    echo '  </td>'."\r\n";
                    echo '</tr>'."\r\n";
            }
            Database::disconnect();
            ?>
            </tbody>
    </table>
    </div>
</div>

<?php include("../includes/footer.php"); ?>
