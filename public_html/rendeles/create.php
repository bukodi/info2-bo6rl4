<?php
     
    require '../includes/database.php';
    if( !empty($_GET) ) {
        $ALKATRESZ_ID = $_GET['alkatresz_id'];
        $RENDELESI_MENNYISEG = $_GET['rendelesi_mennyiseg'];
    }
 
    if ( !empty($_POST)) {
        // keep track validation errors
        $ALKATRESZError = null;
        $RENDELESI_MENNYISEGError = null;
         
        // keep track post values
        $ALKATRESZ_ID = $_POST['ALKATRESZ_ID'];
        $RENDELESI_MENNYISEG = $_POST['RENDELESI_MENNYISEG'];
         
        // validate input
        $valid = true;
        if (empty($ALKATRESZ_ID)) {
            $ALKATRESZError = 'Az alkatrész kiválasztása kötelező';
            $valid = false;
        }
         
        if (empty($RENDELESI_MENNYISEG)) {
            $RENDELESI_MENNYISEGError = 'A rendelési kódmennyiség kitötése kötelező';
            $valid = false;
        }
                  
        // insert data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'INSERT INTO RENDELES ( ALKATRESZ_ID, RENDELESI_MENNYISEG ) values(?, ?)';
            $q = $pdo->prepare($sql);
            $q->execute(array($ALKATRESZ_ID, $RENDELESI_MENNYISEG));
            // TODO: Handling error, e. FK violation
            Database::disconnect();
            header("Location: list.php");
        }
    }
?>

<?php include("../includes/header.php"); ?>

<div class="app-rendeles">
    <div class="row app-page-title"><h3>Új rendeles létrehozása</h3></div>

    <form class="app-page-body form-horizontal" action="create.php" method="post">

        <!-- ALKATRESZ_ID -->
        <div class="control-group <?php echo !empty($ALKATRESZError)?'error':'';?>">
        <label class="control-label">Alkatrész</label>
        <div class="controls">
            <select name="ALKATRESZ_ID">
                <option value="" disabled selected>Alkatrész</option>
                <?php
                $pdo2 = Database::connect();
                $sql2 = 'SELECT ID, MEGNEVEZES FROM ALKATRESZ ORDER BY MEGNEVEZES';
                foreach ($pdo2->query($sql2) as $row2) {
                        $isselected = $row2['ID'] == $ALKATRESZ_ID ? 'selected="selected" ' : '';
                        echo '<option '.$isselected.'value="'.$row2['ID'] .'">'.$row2['MEGNEVEZES'].'</option>'."\r\n";
                }
                Database::disconnect();
                ?>
            </select>
            <?php if (!empty($ALKATRESZError)): ?>
                <span class="help-inline"><?php echo $ALKATRESZError;?></span>
            <?php endif;?>
        </div>
        </div> 

        <!-- RENDELESI_MENNYISEG -->
        <div class="control-group <?php echo !empty($RENDELESI_MENNYISEGError)?'error':'';?>">
        <label class="control-label">Rendelési mennyiség</label>
        <div class="controls">
            <input name="RENDELESI_MENNYISEG" type="text" placeholder="Rendelési mennyiség" value="<?php echo !empty($RENDELESI_MENNYISEG)?$RENDELESI_MENNYISEG:'0';?>">
            <?php if (!empty($RENDELESI_MENNYISEGError)): ?>
                <span class="help-inline"><?php echo $RENDELESI_MENNYISEGError;?></span>
            <?php endif;?>
        </div>
        </div>

        <div class="form-actions app-page-actions">
            <button type="submit" class="btn btn-success">Létrehoz</button>
            <a class="btn" onclick="history.go(-1);">Vissza</a>
        </div>

    </form>
</div>

<?php include("../includes/footer.php"); ?>
