<?php include("../includes/header.php"); ?>
<div class="app-index">
    <div class="row app-page-title"><h1>Raktárkészlet utánpótlás</h1></div>
    <div>
        <h4>Informatika 2 (VIAUAB01) 39. házi feladat</h4>        
        <p><a href="feladat.php">A feladat leírása >>></a></p>
    </div>
    <div><p>&nbsp;</p></div>
    <div>
        <h4>Dokumentáció:</h4>        
        <p><a href="dokumentacio.php">Fejlesztői dokumentáció >>></a></p>
    </div>
    <div>
        <h4>Teszt rendszer:</h4>        
        <p><a href="http://info2-bo6rl4.do.bukodi.com">info2-bo6rl4.do.bukodi.com</a></p>
    </div>
    <div>
        <h4>Forráskód:</h4>        
        <p><a href="https://bitbucket.org/bukodi/info2-bo6rl4">bukodi/info2-bo6rl4 on Bitbucket</a></p>
    </div>
    <div><p>&nbsp;</p></div>
</div>
<?php include("../includes/footer.php"); ?>
