<?php include("../includes/header.php"); ?>
<div class="app-index">
    <div class="row app-page-title"><h1>Raktárkészlet utánpótlás</h1></div>
    <div>
        <h4>Adatbázis</h4>
        <p>Az adatbázis három entitást tartalmaz: ALKATRESZ, BESZALLITO és RENDELES<p>
        <img src="../img/EER-model.png" class="img-polaroid">
        <p>A táblákat tartalmazó sémát, a táblákat és a teszt adatokat létrehozó script a 
        <code>sql/create-schema.sql</code> fileban található.
        <div><p>&nbsp;</p></div>
        <h4>PHP felület</h4>
        <p>A PHP implementácó a Bootstrap CSS library-t használja. Annak érdekében, hogy a header és footer PHP 
            oldalakban levő relatív hivatkozások minden oldalról jól működjenek, ezért az összes oldal egy könyvtár alá került. 
            Így a <code>../</code> relatív hivatkozás mindig a context root-hoz vezet, függetlenül attól, hogy milyen URL 
            path alá telepítették az alkalmazást. A gyökérben levő <code>index.php</code> a <code>home/index.php</code> oldalra irányít át.<p>
        <div><p>&nbsp;</p></div>
        <h4>Fukcionális leírás</h4>
        <p>Az alkalmazás egy teljesen hagyományos CRUD felépítésű felületet követ. Ez annyival lett kiegészítve, 
            hogy az <a href="../alkatresz/list.php">Alkatrészek listája</a> képernyőn követhetjük a készletet és a folyamtban levő 
            rendeléseket is. Ha egy alkatrészből a limit alá csökken a készlet, akkor megjelenik a rendelés gomb, amivel új rendelést 
            hozhatunk létre az adott alkatrészre vonatkozóan.<p>
        <div><p>&nbsp;</p></div>
        <p>Az implementáció kezdeti szakaszában ezt az útmutatót használtam: 
            <a href="http://www.startutorial.com/articles/view/php-crud-tutorial-part-1">PHP CRUD Tutorial (www.startutorial.com)</a></p>
    </div>
</div>
<?php include("../includes/footer.php"); ?>
