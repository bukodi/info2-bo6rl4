<?php include("../includes/header.php"); ?>
<div class="app-index">
    <div class="row app-page-title"><h1>Raktárkészlet utánpótlás</h1></div>
    <div>
        <h4>Informatika 2 (VIAUAB01) 39. házi feladat</h4>
        <p>A rendszer célja, hogy a raktárkészlet aktuális állapota alapján összeállítsa beszállítóink számára a
        rendelési listákat. Ehhez természetesen szükséges a beszállítók adatainak (név, cím, telefonszám),
        illetve az alkatrészek adatainak (név, rendelési kód, néhány katalógusadat, aktuális raktárkészlet,
        utánrendelési küszöb, rendelési mennyiség) kezelése. A megvalósítás során használjuk ki azt, hogy
        adott alkatrészt mindig csak egy beszállítótól szerzünk be.</p>
        <p>A feladat megtervezni és megvalósítani egy webes rendszert, amely az alábbi követelményeknek felel
        meg.</p>
        <p><strong>Rendelések összeállítása</strong><p>
        <ul>
            <li>A rendszer nyitóoldala egy statikus HTML oldal, amelytől a különböző megvalósítandó
            funkciók érhetőek el linkek segítségével.</li>
            <li>Készítsen el egy PHP oldalt, amely listázza név szerint rendezhető módon a rendszerben
            kezelt beszállítókat táblázatos formában.</li>
            <li>Egy beszállítóra kattintva töltsünk be egy oldalt, ahol a beszállító részletes adatai
            megtekinthetők.</li>
            <li>A bevitel gombra kattintva lehessen egy új rendelést megadni a beszállítót felé.</li>
            <li>A törlés gombra kattintva lehessen egy rendelést törölni.</li>
            <li>Készítsen egy oldalt, amire a nyitóoldalról mutasson egy link. Ezen az oldalon keresni lehet a
            beszállítók között és lehessen listázni a a kiválasztott beszállító felé küldendő rendeléseket.</li>
            <li>Készítsen stíluslapot az egyes oldalak egységes megjelenítésének támogatására!</li>
            <li>Minden oldalon legyen egységes fejléc és lábléc. A fejlécben menüként szerepeljen a többi</li>
            megvalósított oldal elérhetősége. A láblécben szerepeljen a feladatot elkészítő hallgató és
            laborvezetőjének adatai.</li>
            <li>Beadandó az elkészített program (az adatbázist létrehozó és minta adatokkal feltöltő script, a
            html, php és css fájlok) és a dokumentáció hozzá (adatbázis terv, rendszerterv, funkcionális
            leírás).</li>
            <li>Az adatbázis az adatokat normalizált formában tárolja</li>
        </ul>
        <p/>
        <p>Forrás: <a href="https://www.aut.bme.hu/Upload/Course/VIAUAB01/publikus_anyagok/H%C3%A1zi%20feladat%20le%C3%ADr%C3%A1sok.pdf">https://www.aut.bme.hu/Upload/Course/VIAUAB01/publikus_anyagok/H%C3%A1zi%20feladat%20le%C3%ADr%C3%A1sok.pdf</a></p>
    </div>
</div>
<?php include("../includes/footer.php"); ?>
