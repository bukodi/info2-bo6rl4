<?php
    require '../includes/database.php';
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: list.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'SELECT a.*, b.NEV BESZALLITO_NEV FROM ALKATRESZ a '
            .'JOIN BESZALLITO b ON a.BESZALLITO_ID = b.ID '  
            .'WHERE a.ID = ?';
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
    }
?>

<?php include("../includes/header.php"); ?>

<div class="app-alkatresz">
    <div class="row app-page-title"><h3>Alkatrész</h3></div>
        
    <div class="app-page-body form-horizontal" >
        <div class="control-group">
        <label class="control-label">Megnevezés</label>
        <div class="controls">
            <label class="checkbox">
                <strong><?php echo $data['MEGNEVEZES'];?></strong>
            </label>
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Leírás</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['LEIRAS'];?>
            </label>
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Beszállító</label>
        <div class="controls">
            <a class="checkbox" href="../beszallito/show.php?id=<?php echo $data['BESZALLITO_ID'];?>"><?php echo $data['BESZALLITO_NEV'];?></a>                
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Rendelési kód</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['RENDELESI_KOD'];?>
            </label>
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Aktuális készlet</label>
        <div class="controls">
            <label class="checkbox">
                <strong><?php echo $data['AKTUALIS_KESZLET'];?></strong>
            </label>
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Utánrendelési küszöb</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['UTANRENDELESI_KUSZOB'];?>
            </label>
        </div>
        </div>

        <div class="control-group">
        <label class="control-label">Rendelési lépcső</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['RENDELESI_LEPCSO'];?>
            </label>
        </div>
        </div>

        <div class="form-actions app-page-actions">
            <a class="btn btn-success" href="update.php?id=<?php echo $_GET['id'];?>">Módosít</a>
            <a class="btn" onclick="history.go(-1);">Vissza</a>
        </div>
    </div>
</div>

<?php include("../includes/footer.php"); ?>
