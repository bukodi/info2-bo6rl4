<?php include("../includes/header.php"); ?>
<div class="app-alkatresz">
    <div class="row app-page-title"><h3>Alkatrészek listája</h3></div>
    <div class="row app-page-body">        
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Megnevezés</th>
                <th>Beszállító</th>
                <th>Készlet</th>
                <th>Küszöb</th>
                <th>Megrendelve</th>
                <th>Rendelés</th>                
                <th><a href="create.php" class="btn btn-success">Új alkatrész</a></th>
            </tr>
            </thead>
            <tbody>
            <?php
            include '../includes/database.php';
            $pdo = Database::connect();
            $sql = 'SELECT a.ID, a.MEGNEVEZES, b.NEV BESZALLITO, b.ID BESZALLITO_ID '
                .', a.AKTUALIS_KESZLET KESZLET, a.UTANRENDELESI_KUSZOB, a.RENDELESI_LEPCSO '
                .', IFNULL((SELECT SUM(RENDELESI_MENNYISEG) FROM RENDELES WHERE ALKATRESZ_ID = a.ID),0) RENDELT_MENNYISEG '
                .', (a.AKTUALIS_KESZLET < a.UTANRENDELESI_KUSZOB) KELL_RENDELNI '
                .'FROM ALKATRESZ a '
                .'JOIN BESZALLITO b ON a.BESZALLITO_ID = b.ID '
                .'ORDER BY a.MEGNEVEZES';
            foreach ($pdo->query($sql) as $row) {                
                    echo '<tr>'."\r\n";
                    echo '  <td>'. $row['MEGNEVEZES'] . '</td>'."\r\n";
                    echo '  <td><a href="../beszallito/show.php?id='.$row['BESZALLITO_ID'].'">'.$row['BESZALLITO'].'</a></td>'."\r\n";
                    echo '  <td>'. $row['KESZLET'] . '</td>'."\r\n";
                    echo '  <td>'. $row['UTANRENDELESI_KUSZOB'] . '</td>'."\r\n";
                    echo '  <td>'. $row['RENDELT_MENNYISEG'] . '</td>'."\r\n";
                    echo '  <td width=80>';
                    echo ' ';
                    if( $row['RENDELT_MENNYISEG'] <> "0" ) {
                        echo '<a class="btn" href="../rendeles/list.php?alkatresz_id='.$row['ID'].'">Lista</a>';
                    } else if ( $row['KELL_RENDELNI'] <> "0") {
                        echo '<a class="btn btn-danger" href="../rendeles/create.php?alkatresz_id='.$row['ID'].'&rendelesi_mennyiseg='.$row['RENDELESI_LEPCSO'].'">Rendel</a>';                        
                    } else {
                        echo 'Ok';
                    }
                    echo '  </td>'."\r\n";
                    echo '  <td width=180>';
                    echo '<a class="btn" href="show.php?id='.$row['ID'].'">Megnéz</a>';
                    echo ' ';
                    echo '<a class="btn btn-success" href="update.php?id='.$row['ID'].'">Módosít</a>';
                    echo '  </td>'."\r\n";
                    echo '</tr>'."\r\n";
            }
            Database::disconnect();
            ?>
            </tbody>
    </table>
    </div>
</div>
<?php include("../includes/footer.php"); ?>
