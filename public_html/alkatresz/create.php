<?php
     
    require '../includes/database.php';
 
    if ( !empty($_POST)) {
        // keep track validation errors
        $MEGNEVEZESError = null;
        $LEIRASError = null;
        $BESZALLITOError = null;
        $RENDELESI_KODError = null;
        $AKTUALIS_KESZLETError = null;
        $UTANRENDELESI_KUSZOBError = null;
        $RENDELESI_LEPCSOError = null;
         
        // keep track post values
        $MEGNEVEZES = $_POST['MEGNEVEZES'];
        $LEIRAS = $_POST['LEIRAS'];
        $BESZALLITO_ID = $_POST['BESZALLITO_ID'];
        $RENDELESI_KOD = $_POST['RENDELESI_KOD'];
        $AKTUALIS_KESZLET = $_POST['AKTUALIS_KESZLET'];
        $UTANRENDELESI_KUSZOB = $_POST['UTANRENDELESI_KUSZOB'];
        $RENDELESI_LEPCSO = $_POST['RENDELESI_LEPCSO'];
         
        // validate input
        $valid = true;
        if (empty($MEGNEVEZES)) {
            $MEGNEVEZESError = 'A megnevezés kitötése kötelező';
            $valid = false;
        }
         
        if (empty($BESZALLITO_ID)) {
            $BESZALLITOError = 'A beszállító kiválasztása kötelező';
            $valid = false;
        }
         
        if (empty($RENDELESI_KOD)) {
            $RENDELESI_KODError = 'A rendelési kód kitötése kötelező';
            $valid = false;
        }
                  
        // insert data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = 'INSERT INTO ALKATRESZ '
                .'( MEGNEVEZES, LEIRAS, BESZALLITO_ID, RENDELESI_KOD, '
                .'AKTUALIS_KESZLET, UTANRENDELESI_KUSZOB, RENDELESI_LEPCSO ) '
                .'values(?, ?, ?, ?, ?, ?, ?)';
            $q = $pdo->prepare($sql);
            $q->execute(array($MEGNEVEZES, $LEIRAS, 
                $BESZALLITO_ID, $RENDELESI_KOD, $AKTUALIS_KESZLET, 
                $UTANRENDELESI_KUSZOB, $RENDELESI_LEPCSO ));
            // TODO: Handling error, e. FK violation
            Database::disconnect();
            header("Location: list.php");
        }
    }
?>

<?php include("../includes/header.php"); ?>

<div class="app-beszallito">
    <div class="row app-page-title"><h3>Új alkatrész létrehozása</h3></div>

    <form class="app-page-body form-horizontal" action="create.php" method="post">

        <!-- MEGNEVEZES -->
        <div class="control-group <?php echo !empty($MEGNEVEZESError)?'error':'';?>">
        <label class="control-label">Megnevezés</label>
        <div class="controls">
            <input name="MEGNEVEZES" type="text" placeholder="Megnevezés" value="<?php echo !empty($MEGNEVEZES)?$MEGNEVEZES:'';?>">
            <?php if (!empty($MEGNEVEZESError)): ?>
                <span class="help-inline"><?php echo $MEGNEVEZESError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- LEIRAS -->
        <div class="control-group <?php echo !empty($LEIRASError)?'error':'';?>">
        <label class="control-label">Leírás</label>
        <div class="controls">
            <input name="LEIRAS" type="text" placeholder="Leírás" value="<?php echo !empty($LEIRAS)?$LEIRAS:'';?>">
            <?php if (!empty($LEIRASError)): ?>
                <span class="help-inline"><?php echo $LEIRASError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- BESZALLITO_ID -->
        <div class="control-group <?php echo !empty($BESZALLITOError)?'error':'';?>">
        <label class="control-label">Beszállító</label>
        <div class="controls">
            <select name="BESZALLITO_ID">
                <option value="" disabled selected>Beszállító</option>
                <?php
                $pdo2 = Database::connect();
                $sql2 = 'SELECT ID, NEV FROM BESZALLITO ORDER BY NEV';
                foreach ($pdo2->query($sql2) as $row2) {
                        $isselected = $row2['ID'] == $BESZALLITO_ID ? 'selected="selected" ' : '';
                        echo '<option '.$isselected.'value="'.$row2['ID'] .'">'.$row2['NEV'].'</option>'."\r\n";
                }
                Database::disconnect();
                ?>
            </select>
            <?php if (!empty($BESZALLITOError)): ?>
                <span class="help-inline"><?php echo $BESZALLITOError;?></span>
            <?php endif;?>
        </div>
        </div> 

        <!-- RENDELESI_KOD -->
        <div class="control-group <?php echo !empty($RENDELESI_KODError)?'error':'';?>">
        <label class="control-label">Rendelési kód</label>
        <div class="controls">
            <input name="RENDELESI_KOD" type="text" placeholder="Rendelési kód" value="<?php echo !empty($RENDELESI_KOD)?$RENDELESI_KOD:'';?>">
            <?php if (!empty($RENDELESI_KODError)): ?>
                <span class="help-inline"><?php echo $RENDELESI_KODError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- AKTUALIS_KESZLET -->
        <div class="control-group <?php echo !empty($AKTUALIS_KESZLETError)?'error':'';?>">
        <label class="control-label">Aktuális készlet</label>
        <div class="controls">
            <input name="AKTUALIS_KESZLET" type="text" placeholder="Aktuális készlet" value="<?php echo !empty($AKTUALIS_KESZLET)?$AKTUALIS_KESZLET:'';?>">
            <?php if (!empty($AKTUALIS_KESZLETError)): ?>
                <span class="help-inline"><?php echo $AKTUALIS_KESZLETError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- UTANRENDELESI_KUSZOB -->
        <div class="control-group <?php echo !empty($UTANRENDELESI_KUSZOBError)?'error':'';?>">
        <label class="control-label">Utánrendelési küszöb</label>
        <div class="controls">
            <input name="UTANRENDELESI_KUSZOB" type="text" placeholder="Utánrendelési küszöb" value="<?php echo !empty($UTANRENDELESI_KUSZOB)?$UTANRENDELESI_KUSZOB:'';?>">
            <?php if (!empty($UTANRENDELESI_KUSZOBError)): ?>
                <span class="help-inline"><?php echo $UTANRENDELESI_KUSZOBError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- RENDELESI_LEPCSO -->
        <div class="control-group <?php echo !empty($RENDELESI_LEPCSOError)?'error':'';?>">
        <label class="control-label">Rendelési lépcső</label>
        <div class="controls">
            <input name="RENDELESI_LEPCSO" type="text" placeholder="Rendelési lépcső" value="<?php echo !empty($RENDELESI_LEPCSO)?$RENDELESI_LEPCSO:'';?>">
            <?php if (!empty($RENDELESI_LEPCSOError)): ?>
                <span class="help-inline"><?php echo $RENDELESI_LEPCSOError;?></span>
            <?php endif;?>
        </div>
        </div>

        <div class="form-actions app-page-actions">
            <button type="submit" class="btn btn-success">Létrehoz</button>
            <a class="btn" onclick="history.go(-1);">Vissza</a>
        </div>

    </form>
</div>

<?php include("../includes/footer.php"); ?>
