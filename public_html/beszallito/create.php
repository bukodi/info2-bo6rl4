<?php
     
    require '../includes/database.php';
 
    if ( !empty($_POST)) {
        // keep track validation errors
        $NEVError = null;
        $TELEFONError = null;
        $CIMError = null;
         
        // keep track post values
        $NEV = $_POST['NEV'];
        $TELEFON = $_POST['TELEFON'];
        $CIM = $_POST['CIM'];
         
        // validate input
        $valid = true;
        if (empty($NEV)) {
            $NEVError = 'A név kitötése kötelező';
            $valid = false;
        }
         
        if (empty($TELEFON)) {
            $TELEFONError = 'A telefonszám kitötése kötelező';
            $valid = false;
        } 
         
        if (empty($CIM)) {
            $CIMError = 'A cím kitötése kötelező';
            $valid = false;
        }
         
        // insert data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO BESZALLITO (NEV,TELEFON,CIM) values(?, ?, ?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($NEV,$TELEFON,$CIM));
            // TODO: Handling error, e. FK violation
            Database::disconnect();
            header("Location: list.php");
        }
    }
?>

<?php include("../includes/header.php"); ?>

<div class="app-beszallito">
    <div class="row app-page-title"><h3>Új beszállító létrehozása</h3></div>

    <form class="app-page-body form-horizontal" action="create.php" method="post">

        <!-- NEV -->
        <div class="control-group <?php echo !empty($NEVError)?'error':'';?>">
        <label class="control-label">Név</label>
        <div class="controls">
            <input name="NEV" type="text" placeholder="Név" value="<?php echo !empty($NEV)?$NEV:'';?>">
            <?php if (!empty($NEVrror)): ?>
                <span class="help-inline"><?php echo $NEVError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- TELEFON -->
        <div class="control-group <?php echo !empty($TELEFONError)?'error':'';?>">
        <label class="control-label">Telefonszám</label>
        <div class="controls">
            <input name="TELEFON" type="text" placeholder="Teelefonszám" value="<?php echo !empty($TELEFON)?$TELEFON:'';?>">
            <?php if (!empty($TELEFONError)): ?>
                <span class="help-inline"><?php echo $TELEFONError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- CIM -->
        <div class="control-group <?php echo !empty($CIMError)?'error':'';?>">
        <label class="control-label">Cím</label>
        <div class="controls">
            <input name="CIM" type="text" placeholder="Cím" value="<?php echo !empty($CIM)?$CIM:'';?>">
            <?php if (!empty($CIMError)): ?>
                <span class="help-inline"><?php echo $CIMError;?></span>
            <?php endif;?>
        </div>
        </div>

        <div class="form-actions app-page-actions">
            <button type="submit" class="btn btn-success">Létrehoz</button>
            <a class="btn" onclick="history.go(-1);">Vissza</a>
        </div>

    </form>
</div>

<?php include("../includes/footer.php"); ?>
