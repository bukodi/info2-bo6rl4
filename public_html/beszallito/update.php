<?php
    require '../includes/database.php';
 
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: list.php");
    }
     
    if ( !empty($_POST)) {
        // keep track validation errors
        $NEVError = null;
        $TELEFONError = null;
        $CIMError = null;
         
        // keep track post values
        $NEV = $_POST['NEV'];
        $TELEFON = $_POST['TELEFON'];
        $CIM = $_POST['CIM'];
         
        // validate input
        $valid = true;
        if (empty($NEV)) {
            $NEVError = 'A név kitötése kötelező';
            $valid = false;
        }
         
        if (empty($TELEFON)) {
            $TELEFONError = 'A telefonszám kitötése kötelező';
            $valid = false;
        } 
         
        if (empty($CIM)) {
            $CIMError = 'A cím kitötése kötelező';
            $valid = false;
        }
         
        // update data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE BESZALLITO SET NEV = ?, TELEFON = ?, CIM =? WHERE id = ?";
            $q = $pdo->prepare($sql);
            $q->execute(array($NEV,$TELEFON,$CIM,$id));
            Database::disconnect();
            header("Location: list.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM BESZALLITO WHERE id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $NEV = $data['NEV'];
        $TELEFON = $data['TELEFON'];
        $CIM = $data['CIM'];
        Database::disconnect();
    }
?>

<?php include("../includes/header.php"); ?>

<div class="app-beszallito">
    <div class="row app-page-title"><h3>Beszállító módosítása</h3></div>

    <form class="app-page-body form-horizontal" action="update.php?id=<?php echo $id?>" method="post">

        <!-- NEV -->
        <div class="control-group <?php echo !empty($NEVError)?'error':'';?>">
        <label class="control-label">Név</label>
        <div class="controls">
            <input name="NEV" type="text" placeholder="Név" value="<?php echo !empty($NEV)?$NEV:'';?>">
            <?php if (!empty($NEVrror)): ?>
                <span class="help-inline"><?php echo $NEVError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- TELEFON -->
        <div class="control-group <?php echo !empty($TELEFONError)?'error':'';?>">
        <label class="control-label">Telefonszám</label>
        <div class="controls">
            <input name="TELEFON" type="text" placeholder="Teelefonszám" value="<?php echo !empty($TELEFON)?$TELEFON:'';?>">
            <?php if (!empty($TELEFONError)): ?>
                <span class="help-inline"><?php echo $TELEFONError;?></span>
            <?php endif;?>
        </div>
        </div>

        <!-- CIM -->
        <div class="control-group <?php echo !empty($CIMError)?'error':'';?>">
        <label class="control-label">Cím</label>
        <div class="controls">
            <input name="CIM" type="text" placeholder="Cím" value="<?php echo !empty($CIM)?$CIM:'';?>">
            <?php if (!empty($CIMError)): ?>
                <span class="help-inline"><?php echo $CIMError;?></span>
            <?php endif;?>
        </div>
        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-success">Módosít</button>
            <a class="btn btn-danger" href="delete.php?id=<?php echo $id;?>">Töröl</a>
            <a class="btn" onclick="history.go(-1);">Vissza</a>
        </div>
    </form>
</div>

<?php include("../includes/footer.php"); ?>
