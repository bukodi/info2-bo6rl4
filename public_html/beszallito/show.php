<?php
    require '../includes/database.php';
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: list.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM BESZALLITO WHERE id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
    }
?>

<?php include("../includes/header.php"); ?>

<div class="app-beszallito">
    <div class="row app-page-title"><h3>Beszállító</h3></div>
        
    <div class="app-page-body form-horizontal" >
        <div class="control-group">
        <label class="control-label">Név</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['NEV'];?>
            </label>
        </div>
        </div>
        <div class="control-group">
        <label class="control-label">Telefonszám</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['TELEFON'];?>
            </label>
        </div>
        </div>
        <div class="control-group">
        <label class="control-label">Cím</label>
        <div class="controls">
            <label class="checkbox">
                <?php echo $data['CIM'];?>
            </label>
        </div>
        </div>
        <div class="form-actions app-page-actions">
            <a class="btn btn-success" href="update.php?id=<?php echo $_GET['id'];?>">Módosít</a>
            <a class="btn" href="../rendeles/list.php?beszallito_id=<?php echo $data['ID'];?>">Rendelések</a>
            <a class="btn" onclick="history.go(-1);">Vissza</a>
        </div>
    </div>
</div>

<?php include("../includes/footer.php"); ?>
