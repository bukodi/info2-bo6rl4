<?php include("../includes/header.php"); ?>
<div class="app-beszallito">
    <div class="row app-page-title"><h3>Beszallítók listája</h3></div>
    <div class="row app-page-body">
        
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Név</th>
                <th>Telefon</th>
                <th>Cím</th>
                <th><a href="create.php" class="btn btn-success">Új beszállító</a></th>
            </tr>
            </thead>
            <tbody>
            <?php
            include '../includes/database.php';
            $pdo = Database::connect();
            $sql = 'SELECT * FROM BESZALLITO '
                . 'ORDER BY NEV ';
            foreach ($pdo->query($sql) as $row) {
                    echo '<tr>'."\r\n";
                    echo '  <td>'. $row['NEV'] . '</td>'."\r\n";
                    echo '  <td>'. $row['TELEFON'] . '</td>'."\r\n";
                    echo '  <td>'. $row['CIM'] . '</td>'."\r\n";
                    echo '  <td width=250>';
                    echo '<a class="btn" href="show.php?id='.$row['ID'].'">Megnéz</a>';
                    echo ' ';
                    echo '<a class="btn btn-success" href="update.php?id='.$row['ID'].'">Módosít</a>';
                    echo ' ';
                    echo '<a class="btn btn-danger" href="delete.php?id='.$row['ID'].'">Töröl</a>';
                    echo '  </td>'."\r\n";
                    echo '</tr>'."\r\n";
            }
            Database::disconnect();
            ?>
            </tbody>
    </table>
    </div>
</div>

<?php include("../includes/footer.php"); ?>
