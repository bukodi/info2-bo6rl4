<?php
class Database
{
    private static $dbName = 'info2_bo6rl4' ;
    private static $dbHost = 'localhost' ;
    private static $dbUsername = 'phpuser';
    private static $dbUserPassword = 'XB6Z2eZ9eTHE';
     
    private static $cont  = null;
     
    public function __construct() {
        die('Init function is not allowed');
    }
     
    public static function connect()
    {
       // One connection through whole application
       if ( null == self::$cont )
       {     
        try
        {
          self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);
          self::$cont->exec("set names utf8");  
        }
        catch(PDOException $e)
        {
          die($e->getMessage()); 
        }
       }
       return self::$cont;
    }
     
    public static function disconnect()
    {
        self::$cont = null;
    }
}
?>