<!DOCTYPE html>
<html lang="en">
<head>
    <title>Rendelés kezelő</title>
    <meta charset="utf-8">
    <link   href="../css/app.css" rel="stylesheet">
    <link   href="../css/bootstrap.css" rel="stylesheet">
    <script src="../js/bootstrap.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
</head>
 
<body>

<div class="container-fluid">
  <div class="row">
    <div class="span2">
        <!--Sidebar content-->
        <div class="row">
            <h1>&nbsp;</h1>
        </div>        
        <ul class="nav nav-pills nav-stacked" >
            <li><a href="../home/index.php" class="list-group-item active">.:: Home ::.</a></li> 
            <li><a href="../rendeles/list.php" class="list-group-item">Rendelések</a></li> 
            <li><a href="../alkatresz/list.php" class="list-group-item">Alkatrészek</a></li> 
            <li><a href="../beszallito/list.php" class="list-group-item">Beszállítók</a></li> 
        </ul>
    </div>
    <div class="span10">
        <!--Body content-->
  
